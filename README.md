![pipeline status](https://gitlab.com/cedric.nicoloso/mevn-enterprise-boilerplate/badges/master/pipeline.svg)

# mevn-enterprise-boilerplate

The code for [Vue.js Enterprise Development Crash Course](https://crash-course.enterprisevue.com/).

## A note of my change

Introducing **cypress.io** to replace nightwatch for e2e testing:
 - Remove nightwatch config
 - Add new vue-cli plugin: @vue/cli-plugin-e2e-cypress
 - One example test
 - All necessary updates to `.gitlab-ci.yml`
 - Update to `HelloWorld.vue` with a link to cypress vue-cli plugin

### Passing CI

![Alt text](green-cypress-test.png?raw=true 'Green cypress test')

### Resources:

 - vue-cli plugin:
https://www.npmjs.com/package/@vue/cli-plugin-e2e-cypress  
https://github.com/vuejs/vue-cli/tree/dev/packages/@vue/cli-plugin-e2e-cypress  
 - Help to configure Gitlab CI:
https://github.com/cypress-io/cypress-example-kitchensink/blob/master/.gitlab-ci.yml

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

## Deployment

- Create a new Heroku app
- In Gitlab UI go to *Settings > CI > Variables* and set `HEROKU_APP_NAME` and `HEROKU_API_KEY`.
